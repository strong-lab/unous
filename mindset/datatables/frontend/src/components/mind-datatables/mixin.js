const Mixin = {
  data: () => ({}),

  computed: {
    hasFixed: (payload) => {
        const hasFreeze = Object
                            .keys(payload.model.settings)
                            .map(field => payload.model.settings[field].hasOwnProperty('freeze') && payload.model.settings[field].freeze)
                            .some(elem => elem);

        return hasFreeze;
    },

    // renderHeaderFixed: (payload) => {
        

    // }
  },

  methods: {
    checkAll: payload => {
      console.log(payload, payload.srcElement.checked);
    }
  }
};

export default Mixin;
