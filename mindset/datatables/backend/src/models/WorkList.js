const randomDate = (start, end) => new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));

const randomNumber = number => parseInt(Math.random() * number) + 1;

const listRandomClient = ["Adidas", "Nike", "Centauro", "Riachuelo", "TNG", "Cacau Show"];
const listRandomDescriptionModelCor = ["Blusa", "Shorts", "Calça", "Tenis", "Saia", "Vestido", "Bola"];
const listRandoncluster = ["Sul", "Centro", "Nordeste", "Lojas GG", "Lojas Low", "Lojas Quentes", "Lojas Shopping"];
const listRandomItemMinMax = ["Min 1 Todas", "Máximo 3 todas", ""];
const listRandomStatusAllocation = ["Calculado", "Calcular", "Aprovado", "Não Calcular", "Processado",];
const listRandomOrderStatus = ["Carteira", "Cancelado", "Recebido", "Armazenado no CD"];
const listRandomAllocationType = ["Automática", "Manual"];
const listRandomNationalImported = ["Nacional", "Importado"];
const listRandomAllocationInformation = ["Não alocar até dia xx/xx/xxxx", ""]

const workList = () => {

    return new Promise((resolve, reject) => setTimeout(() => {
        try {

            let newLine = [];

            for (let index = 0; index < 50; index++) {
                newLine.push({
                    _attribute: {
                        id: Math.random(),
                        select: false,
                    },
                    DeliveryDate: randomDate(new Date(2019, 0, 1), new Date()),
                    ReceiptDate: randomDate(new Date(2019, 0, 1), new Date()),
                    Provider: listRandomClient[randomNumber(6)],
                    OriginCD: "CD00" + randomNumber(10),
                    Request: "P00" + randomNumber(10),
                    ModelColor: "M00" + randomNumber(10),
                    DescriptionModelColor: listRandomDescriptionModelCor[randomNumber(7)],
                    Cluster: listRandoncluster[randomNumber(7)],
                    ClusterQuadrant: "<button>",
                    ItemMinMax: listRandomItemMinMax[randomNumber(3)],
                    AllocationStatus: listRandomStatusAllocation[randomNumber(5)],
                    OrderStatus: listRandomOrderStatus[randomNumber(4)],
                    AllocationType: listRandomAllocationType[randomNumber(2)],
                    PercentAllocatedStore: randomNumber(100),
                    Note: "<button>",
                    CostPrice: randomNumber(parseFloat(150.55)),
                    SalesPrice: randomNumber(parseFloat(150.55)),
                    TotalAmount: randomNumber(parseFloat(200.55)),
                    Coordinate: "M00" + randomNumber(10),
                    NationalImported: listRandomNationalImported[randomNumber(2)],
                    AllocationInformation: listRandomAllocationInformation[randomNumber(2)],
                    "Custom03": '',
                    "Custom04": '',
                    "Custom05": ''
                })
            }

            resolve({
                header: [
                    [
                        { label: '',                        fields: '_attribute'},
                        { label: 'Data de Entrega',         fields: 'DeliveryDate'},
                        { label: 'Data de Recebimento',     fields: 'ReceiptDate'},
                        { label: 'Fornecedor',              fields: 'Provider'},
                        { label: 'CD Origem',               fields: 'OriginCD'},
                        { label: 'Pedido',                  fields: 'Request'},
                        { label: 'Modelo/Cor',              fields: 'ModelColor'},
                        { label: 'Descrição Modelo/Cor',    fields: 'DescriptionModelColor'},
                        { label: 'Cluster',                 fields: 'Cluster'},
                        { label: 'Quadrante Cluster',       fields: 'ClusterQuadrant'},
                        { label: 'Item Min Max',            fields: 'ItemMinMax'},
                        { label: 'Status Alocação',         fields: 'AllocationStatus'},
                        { label: 'Status Pedido',           fields: 'OrderStatus'},
                        { label: 'Tipo Alocação',           fields: 'AllocationType'},
                        { label: '% Alocado Loja',          fields: 'PercentAllocatedStore'},
                        { label: 'Observação',              fields: 'Note'},
                        { label: 'Preço de Custo',          fields: 'CostPrice'},
                        { label: 'Preço de Venda',          fields: 'SalesPrice'},
                        { label: 'Quantidade Total',        fields: 'TotalAmount'},
                        { label: 'Coordenado',              fields: 'Coordinate'},
                        { label: 'Nacional Importado',      fields: 'NationalImported'},
                        { label: 'Informações Alocação',    fields: 'AllocationInformation'},
                        { label: 'Custom 03',               fields: 'Custom03'},
                        { label: 'Custom 04',               fields: 'Custom04'},
                        { label: 'Custom 05',               fields: 'Custom05'},
                    ]
                ],
                body: newLine
            })
        }
        catch(err) {
            reject({message: 'Error: Bad request response'});
        }
    }, 500));
}

module.exports = workList;