const { Router } = require('express');
const WorkListController = require('./controller/WorkListController');

const routes = Router();

routes.get('/', (resquest, response) => {
    return response.json({
        Api: 'Online'
    })
});

routes.get('/work-list', WorkListController.index);

module.exports = routes;