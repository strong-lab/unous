const WorkList = require('../models/WorkList');

module.exports = {
    async index(request, response){
        const apiResponse = await WorkList();
        return response.json(apiResponse);
    },
}